Data in excel Files:

1. Batches (batch.xlsx)
    | batch_id | batch_name |
    -----------+-------------
    |    1     |    4th     |
    |    2     |    5th     |
    |    3     |    6th     |
    |    4     |    7th     |
    |    5     |    8th     |

2. Subjects (subject.xlsx)
    | subject_id | subject_name |
    -------------+---------------
    |      1     |     Maths    |
    |      2     |    English   |
    |      3     |    Physics   |
    |      4     |   Chemistry  |
    |      5     |    Biology   |

3. Students (student.xlsx)
    | student_id | student_name | mobile_no  |    email_id     | batch_id |
    -------------+--------------+------------+-----------------+-----------
    |      1     | Vishal       | 1234567890 | abc@7classes.in |    1     |
    |      2     | Aditya       | 1234567890 | abc@7classes.in |    7     |
    |      3     | Rajat        | 1234567890 | abc@7classes.in |    10    |
    |      4     | Vivek        | 1234567890 | abc@7classes.in |    9     |

4. Teachers (teacher.xlsx)
    | teacher_id | teacher_name | mobile_no  |    email_id     | subject_id |
    -------------+--------------+------------+-----------------+-------------
    |    1       | abc          | 1234567890 | xyz@7classes.in |     1      |
    |    2       | cde          | 1234567890 | xyz@7classes.in |     3      |
    |    3       | efg          | 1234567890 | xyz@7classes.in |     1      |
    |    4       | ghi          | 1234567890 | xyz@7classes.in |     3      |

5. Availability (availability.xlsx)
    | teacher_id | subject_name | class_day | class_time |
    -------------+--------------+-----------+------------|
    |    1       | Maths        | monday    | 12-13      |
    |    1       | Maths        | monday    | 9-10       |
    |    2       | English      | monday    | 14-15      |
    |    2       | English      | thursday  | 12-13      |
    |    2       | English      | saturday  | 12-13      |
    |    2       | English      | saturday  | 10-11      |
    |    3       | Physics      | thursday  | 11-12      |
    |    3       | Physics      | thursday  | 9-10       |
    |    4       | Chemistry    | tuesday   | 14-15      |
    |    4       | Chemistry    | tuesday   | 12-13      |
    |    4       | Chemistry    | friday    | 11-12      |
    |    4       | Chemistry    | friday    | 9-10       |

    
Steps to Book the Slot:-
1. Run the data.py File
2. You'll get the Available and Booked slot list
3. Enter Student ID
4. Enter Teacher ID
5. Enter Subejct Name
6. Enter Class day
7. Enter Class Timing
8. Slot Booked! :)
