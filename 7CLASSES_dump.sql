--
-- PostgreSQL database dump
--

-- Dumped from database version 13.4
-- Dumped by pg_dump version 13.4

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: adminpack; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS adminpack WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION adminpack; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION adminpack IS 'administrative functions for PostgreSQL';


SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: availability; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.availability (
    teacher_id integer,
    subject_name character varying,
    class_day character varying,
    class_time character varying
);


ALTER TABLE public.availability OWNER TO postgres;

--
-- Name: batch; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.batch (
    batch_id integer NOT NULL,
    batch_name character varying
);


ALTER TABLE public.batch OWNER TO postgres;

--
-- Name: booked; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.booked (
    student_id integer,
    teacher_id integer,
    subject_name character varying,
    class_day character varying,
    class_time character varying
);


ALTER TABLE public.booked OWNER TO postgres;

--
-- Name: student; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.student (
    student_id integer NOT NULL,
    student_name character varying,
    mobile_no integer,
    email_id character varying,
    batch_id integer
);


ALTER TABLE public.student OWNER TO postgres;

--
-- Name: subject; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.subject (
    subject_id integer NOT NULL,
    subject_name character varying
);


ALTER TABLE public.subject OWNER TO postgres;

--
-- Name: teacher; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.teacher (
    teacher_id integer NOT NULL,
    teacher_name character varying,
    mobile_no integer,
    email_id character varying,
    subject_id integer
);


ALTER TABLE public.teacher OWNER TO postgres;

--
-- Data for Name: availability; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.availability (teacher_id, subject_name, class_day, class_time) FROM stdin;
1	Maths	monday	9-10
1	Maths	wednesday	14-15
1	Maths	wednesday	9-10
1	Maths	saturday	14-15
1	Maths	saturday	9-10
2	English	monday	14-15
2	English	thursday	15-16
2	English	thursday	11-12
2	English	thursday	12-13
2	English	saturday	12-13
3	Physics	thursday	11-12
3	Physics	thursday	9-10
3	Physics	saturday	13-14
3	Physics	saturday	11-12
4	Chemistry	tuesday	14-15
4	Chemistry	tuesday	12-13
4	Chemistry	friday	11-12
4	Chemistry	friday	9-10
1	Maths	monday	12-13
2	English	monday	11-12
2	English	saturday	10-11
\.


--
-- Data for Name: batch; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.batch (batch_id, batch_name) FROM stdin;
1	4th
2	5th
3	6th
4	7th
5	8th
6	9th
7	10th
8	11th
9	12th
10	IIT
11	NEET
\.


--
-- Data for Name: booked; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.booked (student_id, teacher_id, subject_name, class_day, class_time) FROM stdin;
1	1	Maths	monday	12-13
1	2	English	monday	11-12
2	2	English	saturday	10-11
\.


--
-- Data for Name: student; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.student (student_id, student_name, mobile_no, email_id, batch_id) FROM stdin;
1	Vishal	1234567890	abc@7classes.in	1
2	Aditya	1234567890	abc@7classes.in	7
3	Rajat	1234567890	abc@7classes.in	10
4	Vivek	1234567890	abc@7classes.in	9
\.


--
-- Data for Name: subject; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.subject (subject_id, subject_name) FROM stdin;
1	Maths
2	English
3	Physics
4	Chemistry
5	Biology
\.


--
-- Data for Name: teacher; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.teacher (teacher_id, teacher_name, mobile_no, email_id, subject_id) FROM stdin;
1	abc	1234567890	xyz@7classes.in	1
2	cde	1234567890	xyz@7classes.in	3
3	efg	1234567890	xyz@7classes.in	1
4	ghi	1234567890	xyz@7classes.in	3
\.


--
-- Name: batch batch_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.batch
    ADD CONSTRAINT batch_pkey PRIMARY KEY (batch_id);


--
-- Name: student student_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.student
    ADD CONSTRAINT student_pkey PRIMARY KEY (student_id);


--
-- Name: subject subject_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.subject
    ADD CONSTRAINT subject_pkey PRIMARY KEY (subject_id);


--
-- Name: teacher teacher_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.teacher
    ADD CONSTRAINT teacher_pkey PRIMARY KEY (teacher_id);


--
-- Name: student student_batch_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.student
    ADD CONSTRAINT student_batch_id_fkey FOREIGN KEY (batch_id) REFERENCES public.batch(batch_id);


--
-- Name: teacher teacher_subject_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.teacher
    ADD CONSTRAINT teacher_subject_id_fkey FOREIGN KEY (subject_id) REFERENCES public.subject(subject_id);


--
-- PostgreSQL database dump complete
--

